**With those files, you can easily create and maintain a Nextcloud server with Docker.**

*more explainations in french* : <https://vicet.eu/tuto-installer-nextcloud/>

# Quick Installation

First you can clone the repository. The directory has to be named `nextcloud` to make the scripts working :

```
git clone https://gitlab.com/dvicet/nextcloud-collabora-docker-and-borg-backup-scripts.git nextcloud
```

Then, create a `.env` file containing these environment variables (look at `.env.example`):

* `NEXTCLOUD_URL` : your Nextcloud **domain**
* `DB_ROOT_PASSWORD` : A password for the **root** user of **MariaDB**
* `DB_PASSWORD` : A password for the **nextcloud** user of **MariaDB**
* `BORG_REPO` : The path to your **Borg Backup repository**
* `BORG_PASSPHRASE` : A **big passphrase** to encrypt your **Borg repository**
* `TIMEZONE` : A Timezone for borgmatic dates formatting
* `NEXTCLOUD_AUTH_TOKEN` : Nextcloud token for metrics, generate it with : `occ config:app:set serverinfo token --value yourtoken`

Launch the services with :

```
sudo docker-compose up -d
```

You can then install Nextcloud with the web based installer.
In MariaDB parameters, enter :
* `nextcloud` : for database name and username
* the database password (`DB_PASSWORD` in your `.env` file)
* `db` : for the host

# Backup and update

Generate an ssh key in the `ssh` directory with a command like :

```
ssh-keygen -t ed25519 -f ssh/id_ed25519 -N ''
```

Execute the file `nextcloud_backup_and_update.sh` to automatically backup and update your services. You can regularly execute this file with cron.

# occ commands

The file `occ.sh` helps you to launch occ commands more easily.

```
./occ.sh <command>
```
