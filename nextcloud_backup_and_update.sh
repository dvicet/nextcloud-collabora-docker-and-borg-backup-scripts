#!/bin/bash

# usefull for Docker rootless
export DOCKER_HOST=unix:///run/user/$(id --user)/docker.sock

cd $(dirname $0)

fail() {
	curl \
		-H "Authorization: Bearer ${NTFY_INFRA_TOKEN}" \
		-d "Nextcloud failed to backup" https://${NTFY_DOMAIN}/infra
	endBackup
	exit 1
}

endBackup() {
	# Disable maintenance mode
	docker compose exec -T --user www-data app php occ maintenance:mode --off
	if [ $? -ne 0 ]; then
		exit 4
	fi
}

export $(cat .env | xargs)

# Enable maintenance mode
docker compose exec -T --user www-data app php occ maintenance:mode --on
if [ $? -ne 0 ]; then
	fail
fi

# borgmatic backup
docker compose -f compose.borgmatic.yml pull
docker compose -f compose.borgmatic.yml up --exit-code-from borgmatic
if [ $? -ne 0 ]; then
	echo "[$(date)]" "Borg backup failed"
	fail
fi

echo "[$(date)]" "Files backed up successfully"

# Update Nextcloud
docker compose pull
docker compose build --pull
docker compose up -d
if [ $? -ne 0 ]; then
	fail
fi
echo "[$(date)]" "Nextcloud updated"

endBackup
